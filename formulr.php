<?php

if (isset($_POST["email"]) && isset($_POST["message"]) && isset($_POST["sujet"])) {

    if (!empty($_POST["email"]) && !empty($_POST["message"]) && !empty($_POST["sujet"])) {

        $nom = $_POST["nom"];

        $prenom = $_POST["prenom"];

        $telephone = $_POST["telephone"];

        $email = $_POST["email"];

        $sujet = $_POST["sujet"];

        $petitMot = $_POST["message"];

        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {

            if (filter_var($email, FILTER_SANITIZE_EMAIL)) {

                if (filter_var($petitMot, FILTER_SANITIZE_FULL_SPECIAL_CHARS)) {

                    $petitMot = filter_var($petitMot, FILTER_SANITIZE_FULL_SPECIAL_CHARS);

                    $email = filter_var($email, FILTER_SANITIZE_EMAIL);
                    $sujet = filter_var($sujet, FILTER_SANITIZE_STRING);

                    if (isset($prenom)) {

                        if (!empty($prenom)) {

                            $prenom = filter_var($prenom, FILTER_SANITIZE_STRING);
                        }
                    }

                    if (isset($nom)) {

                        if (!empty($nom)) {

                            $nom = filter_var($nom, FILTER_SANITIZE_STRING);
                        }
                    }

                    if (isset($telephone)) {

                        if (!empty($telephone)) {

                            filter_var($telephone, FILTER_SANITIZE_STRIPPED, array("options" => array('min_range' => 10, 'max_range' => 10)));

                            $telephone = filter_var($telephone, FILTER_SANITIZE_STRIPPED, array("options" => array('min_range' => 10, 'max_range' => 10)));
                        }
                    }
try
{
	$bdd = new PDO('mysql:host=localhost;dbname=form;charset=utf8', 'root', '');
}
catch (Exception $erreur)
{
        die('Erreur : ' . $erreur->getMessage());
}
$AddMail = $bdd->prepare("INSERT INTO t_email (Email) VALUES (:variable_email)");
$emailID = $bdd->prepare("SET @variable_emailID = (SELECT id_Email FROM t_email WHERE Email = :variable_email)");
$AddMsg = $bdd->prepare("INSERT INTO t_msg (sujet, msg, id_Email) VALUES (:variable_sujet, :variable_msg, @variable_emailID)");
$AddPersonnes = $bdd->prepare("INSERT INTO t_personnes (prenom, nom, tel, id_Email) VALUES (:variable_prenom, :variable_nom, :variable_tel, @variable_emailID) ");

try
{

    $mailreçu=[":variable_email" =>$email];
    $messagebdd=[":variable_sujet" =>$sujet, ":variable_msg" =>$petitMot];
    $personnes=[":variable_prenom" =>$prenom, ":variable_nom" =>$nom, ":variable_tel" =>$telephone];
    $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $checkreq= $bdd->prepare("SELECT Email FROM t_email");
    $checkreq->execute();

foreach($checkreq->fetchall() as $recupmail){

    if ($recupmail=$email){
        $checkmail=true;
    }
}

if($checkmail=true){

    $bdd->beginTransaction();
    $emailID->execute($mailreçu);
    $AddMsg->execute($messagebdd);
    $AddPersonnes->execute($personnes);
    $bdd->commit();
 
}else{

    $bdd->beginTransaction();
    $AddMail->execute($mailreçu);
    $emailID->execute($mailreçu);
    $AddMsg->execute($messagebdd);
    $AddPersonnes->execute($personnes);
    
    $bdd->commit();
}
}
catch (Exception $erreur)
{
    printf($erreur->getMessage());
    $bdd->rollBack();
   
}

$to      = "anthony.lioux@laposte.net";

$subject = "Message de la part de $email";

$message = $petitMot;

$headers = "From: anthony.lioux@labo-ve.fr" . PHP_EOL . 'Content-type: text/html; charset=utf-8' . PHP_EOL .

    'X-Mailer: PHP/' . phpversion();

if (!mail($to, $subject, $message, $headers)) {

  printf("Mail non envoyé");

}

                }
            }
        }
    }else  {
        printf("Merci de emplir les champs obligatoires");
    }
} else {
    printf("Interdit de modifier les champs du formulaire");
}


?>